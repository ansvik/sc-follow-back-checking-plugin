var length = 0;
var subscribers = '';
var user;

var service = function()
{
    var z = window.setInterval(function() 
    {
		var elements = document.getElementsByClassName('userBadgeListItem__heading sc-type-small sc-link-dark sc-truncate');
		elements = Object.values(elements);
		if (elements.length != length)
		{
			elements.forEach(function(item)
			{
				if (item.innerHTML.indexOf('подписан') == -1)
				{
					if (subscribers.includes(item.innerHTML.trim())) 
					{
						item.innerHTML += ' подписан';
					}
					else
					{
						item.innerHTML += ' не подписан';	
					}
				}
			});
			length = elements.length;
		}
    }, 1000);
}

var sortFollowers = function()
{
	subscribers = subscribers['collection'];
	var temp = {};
	for (var i = 0; i < subscribers.length; i++) {
		temp[i] = String(subscribers[i]['username']).trim();
	}
	temp = Object.values(temp);
	subscribers = temp;

	/*
	for (var i = 0; i < subscribers.length; i++) {
		alert(subscribers[i]);
	}
	*/

}

var getFollowers = function()
{
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'https://api.soundcloud.com/users/' + user['id'] + '/followers?client_id=rZY6FYrMpGVhVDfaKEHdCaY8ALekxd8P');
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.onload = function (){
		window.onload = function()
		{
			subscribers = JSON.parse(xhr.responseText);
			sortFollowers();
			service();
		}
	}
	xhr.send();
}

var loc = String(window.location);
loc = loc.search(/soundcloud[.]com/ui);

if (loc == -1)
{
	throw("loc-1");
}

user = String(window.location);
user = user.replace(/https:[/][/]soundcloud[.]com[/]/ui, '');
user = user.replace(/[/].*/ui, '');

var xhr = new XMLHttpRequest();
xhr.open('GET', 'https://api.soundcloud.com/users/' + user + '?client_id=rZY6FYrMpGVhVDfaKEHdCaY8ALekxd8P');
xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
xhr.onload = function (){
	subscribers = this.responseText;
	user = JSON.parse(subscribers);
	getFollowers();
}
xhr.send();
